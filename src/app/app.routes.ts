import { HomeComponent } from './components/home/home.component';
import { Routes } from '@angular/router';

export const routes: Routes = [


    {
        path: 'home',
        component: HomeComponent,
        loadChildren: () => import('./components/home/home.routes')
    },
    {
        path: 'card',
        loadComponent: () => import('./components/card/card.component').then(c => c.CardComponent),
        title: "Points fidélité",
    },
    {
        path: 'tacos',
        loadComponent: () => import('./fast-link/tacos/tacos.component').then(c => c.TacosComponent)
    },
    {
        path: 'menus',
        loadComponent: () => import('./fast-link/menus/menus.component').then(c => c.MenusComponent)
    },
    {
        path: 'burgers',
        loadComponent: () => import('./fast-link/burgers/burgers.component').then(c => c.BurgersComponent)
    },
    {
        path: 'wraps',
        loadComponent: () => import('./fast-link/wraps/wraps.component').then(c => c.WrapsComponent)
    },
    {
        path: 'sides',
        loadComponent: () => import('./fast-link/sides/sides.component').then(c => c.SidesComponent)
    },
    {
        path: 'desserts',
        loadComponent: () => import('./fast-link/desserts/desserts.component').then(c => c.DessertsComponent)
    },
    {
        path: 'profil',
        loadComponent: () => import('./components/profil/profil.component').then(c => c.ProfilComponent)
    },
    {
        path: 'cart',
        loadComponent: () => import('./components/cart/cart.component').then(c => c.CartComponent)
    },

    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
    }
];
