import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { MatTabsModule } from '@angular/material/tabs';
import { Sides } from '../../components/sides-details/sides';
import { SidesDetailsComponent } from '../../components/sides-details/sides-details.component';
import { SidesInterface } from '../../utils/side.interface';

@Component({
  selector: 'app-sides',
  standalone: true,
  imports: [CommonModule, MatTabsModule, SidesDetailsComponent],
  templateUrl: './sides.component.html',
  styleUrl: './sides.component.scss'
})
export class SidesComponent {
  sides: SidesInterface[] = Sides;
}
