import { BurgerInterface } from '../../utils/burger.interface';
import { Burgers } from '../../components/burgers-details/burgers';
import { BurgersDetailsComponent } from '../../components/burgers-details/burgers-details.component';
import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { MatTabsModule } from '@angular/material/tabs';

@Component({
  selector: 'app-burgers',
  standalone: true,
  imports: [CommonModule, MatTabsModule, BurgersDetailsComponent],
  templateUrl: './burgers.component.html',
  styleUrl: './burgers.component.scss'
})
export class BurgersComponent {
  burgers: BurgerInterface[] = Burgers
}
