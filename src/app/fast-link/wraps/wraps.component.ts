import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { MatTabsModule } from '@angular/material/tabs';
import { WrapInterface } from '../../utils/wrap.interface';
import { Wraps } from '../../components/wraps-details/wraps';
import { WrapsDetailsComponent } from '../../components/wraps-details/wraps-details.component';

@Component({
  selector: 'app-wraps',
  standalone: true,
  imports: [CommonModule, MatTabsModule, WrapsDetailsComponent],
  templateUrl: './wraps.component.html',
  styleUrl: './wraps.component.scss'
})
export class WrapsComponent {
  wraps: WrapInterface[] = Wraps
}
