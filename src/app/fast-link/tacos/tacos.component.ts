import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { MatTabsModule } from '@angular/material/tabs';
import { Tacos } from '../../components/tacos-details/tacos';
import { TacosDetailsComponent } from '../../components/tacos-details/tacos-details.component';
import { TacosInterface } from '../../utils/tacos-details';

@Component({
  selector: 'app-tacos',
  standalone: true,
  imports: [CommonModule, MatTabsModule, TacosDetailsComponent],
  templateUrl: './tacos.component.html',
  styleUrl: './tacos.component.scss'
})
export class TacosComponent {
  tacos: TacosInterface[] = Tacos;

}
