import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TacosDetailsComponent } from './tacos-details.component';

describe('TacosDetailsComponent', () => {
  let component: TacosDetailsComponent;
  let fixture: ComponentFixture<TacosDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TacosDetailsComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TacosDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
