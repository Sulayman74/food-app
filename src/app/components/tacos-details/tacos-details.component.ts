import { Component, Input } from '@angular/core';

import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { TacosInterface } from '../../utils/tacos-details';

@Component({
  selector: 'app-tacos-details',
  standalone: true,
  imports: [CommonModule, MatButtonModule, MatCardModule, MatIconModule],
  templateUrl: './tacos-details.component.html',
  styleUrl: './tacos-details.component.scss'
})
export class TacosDetailsComponent {
  @Input() taco!: TacosInterface;
}
