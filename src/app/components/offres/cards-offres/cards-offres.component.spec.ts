import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardsOffresComponent } from './cards-offres.component';

describe('CardsOffresComponent', () => {
  let component: CardsOffresComponent;
  let fixture: ComponentFixture<CardsOffresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CardsOffresComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CardsOffresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
