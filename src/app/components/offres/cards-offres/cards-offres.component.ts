import { Component, Input } from '@angular/core';

import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from "@angular/material/card"
import { MatIconModule } from '@angular/material/icon';

@Component({
  selector: 'app-cards-offres',
  standalone: true,
  imports: [CommonModule, MatCardModule, MatButtonModule, MatIconModule],
  templateUrl: './cards-offres.component.html',
  styleUrl: './cards-offres.component.scss'
})
export class CardsOffresComponent {
  @Input() offer!: { image: string; title: string; description: string };
}
