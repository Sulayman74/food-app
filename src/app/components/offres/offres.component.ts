import { Component, OnInit } from '@angular/core';
import { OFFERS, Offer } from '../../utils/offres';

import { CardsOffresComponent } from './cards-offres/cards-offres.component';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-offres',
  standalone: true,
  imports: [CardsOffresComponent, CommonModule],
  templateUrl: './offres.component.html',
  styleUrl: './offres.component.scss'
})
export class OffresComponent implements OnInit {

  offers: Offer[] = [];

  ngOnInit(): void {
    this.offers = OFFERS
  }

}
