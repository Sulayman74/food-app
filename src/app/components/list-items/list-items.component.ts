import { CommonModule, registerLocaleData } from '@angular/common';
import { Component, LOCALE_ID, OnInit } from '@angular/core';

import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { PRODUCTS } from '../../utils/product';
import { Product } from '../../utils/product';
import localeFr from '@angular/common/locales/fr';

registerLocaleData(localeFr, 'fr');


@Component({
  selector: 'app-list-items',
  standalone: true,
  imports: [CommonModule, MatCardModule, MatButtonModule],
  templateUrl: './list-items.component.html',
  styleUrl: './list-items.component.scss',
  providers: [
    { provide: LOCALE_ID, useValue: 'fr' }
  ],
})
export class ListItemsComponent implements OnInit {

  products!: Product[]
  ngOnInit(): void {

    this.products = PRODUCTS
  }

}
