import { Component, EventEmitter, Output, inject } from '@angular/core';
import { Router, RouterModule } from '@angular/router';

import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button'
import { MatIconModule } from '@angular/material/icon'
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';

@Component({
  selector: 'app-header',
  standalone: true,
  imports: [CommonModule, MatToolbarModule, MatIconModule, MatSidenavModule, MatButtonModule, RouterModule],
  templateUrl: './header.component.html',
  styleUrl: './header.component.scss'
})
export class HeaderComponent {
  @Output() toggleDrawer: EventEmitter<void> = new EventEmitter<void>();
  // menuColor: string = "#87cf66"
  onToggleDrawer(): void {
    this.toggleDrawer.emit();
  }
  private router = inject(Router)

  onProfil(): void {
    this.router.navigate(['/profil'])

  }
  onCart(): void {
    this.router.navigate(['/cart'])

  }
}
