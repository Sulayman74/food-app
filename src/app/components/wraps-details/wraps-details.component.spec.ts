import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WrapsDetailsComponent } from './wraps-details.component';

describe('WrapsDetailsComponent', () => {
  let component: WrapsDetailsComponent;
  let fixture: ComponentFixture<WrapsDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [WrapsDetailsComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(WrapsDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
