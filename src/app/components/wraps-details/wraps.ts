import { WrapInterface } from "../../utils/wrap.interface";

export const Wraps: WrapInterface[] = [{
    name: 'Chicken', description: "Tenders, cheddar, salade,tomate,galette de PDT et sauce tartare", price: '6.90', imageUrl: '/assets/images/wrapchicken.jpg'
}, { name: ' Beef', description: "2 Steak 45g, cheddar, cornichon, émincé d'oignon et sauce moutarde-ketchup", price: '6.90', imageUrl: '/assets/images/wrapbeef.jpg' }
]