import { Component, Input } from '@angular/core';

import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { WrapInterface } from '../../utils/wrap.interface';

@Component({
  selector: 'app-wraps-details',
  standalone: true,
  imports: [CommonModule, MatButtonModule, MatCardModule, MatIconModule],
  templateUrl: './wraps-details.component.html',
  styleUrl: './wraps-details.component.scss'
})
export class WrapsDetailsComponent {
  @Input() wrap!: WrapInterface;
}
