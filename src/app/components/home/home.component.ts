import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { ListItemsComponent } from '../list-items/list-items.component';
import { OffresComponent } from '../offres/offres.component';
import { ReviewsComponent } from '../reviews/reviews.component';
import { RouterModule } from '@angular/router';
import { SlideInterface } from '../../utils/slider.interface';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [ListItemsComponent, ReviewsComponent, CommonModule, OffresComponent, RouterModule],
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss'
})
export class HomeComponent {
  slides: SlideInterface[] = [
    { avatarUrl: '/assets/avatars/woman-1.png', username: "Jane", title: 'Burgers incroyables', review: 'Les burgers ici sont tout simplement incroyables ! Juteux, savoureux et cuits à la perfection. Hautement recommandé !', stars: 5 },
    { avatarUrl: '/assets/avatars/woman-2.png', username: "Jessica", title: 'Déçu par les Tacos', review: 'J\'ai essayé les tacos et j\'étais un peu déçu. La garniture était fade et manquait de peps.', stars: 2 },
    { avatarUrl: '/assets/avatars/man-2.png', username: "Henry", title: 'Wraps délicieux', review: 'Les wraps sont délicieux et bien garnis. Une excellente option pour un repas rapide.', stars: 4 },
    { avatarUrl: '/assets/avatars/homme-1.png', username: "Hamid", title: 'Service au top', review: 'Le service était exceptionnel, rapide et très amical. J\'y retournerai sans hésiter.', stars: 5 },
    { title: 'Tacos savoureux', review: 'Les tacos sont vraiment savoureux avec des ingrédients frais. Une belle découverte.', stars: 4 },
    { title: 'Wraps moyens', review: 'Les wraps étaient moyens, pas assez de saveur à mon goût. Peut mieux faire.', stars: 3 },
    { avatarUrl: '/assets/avatars/woman-2.png', username: "Hawa", title: 'Burgers à tomber', review: 'Les burgers sont à tomber par terre, tellement bons ! Une expérience à refaire.', stars: 5 },
    { avatarUrl: '/assets/avatars/man-2.png', username: "Bilal", title: 'Tacos moyens', review: 'Les tacos étaient corrects, mais rien d\'extraordinaire. Un peu déçu.', stars: 3 },
    { title: 'Wraps fantastiques', review: 'Les wraps sont fantastiques et très copieux. Je recommande fortement.', stars: 5 },
    { title: 'Service rapide', review: 'Le service est rapide et efficace. Les burgers sont également excellents.', stars: 4 }
  ];
}
