import { HomeComponent } from './home.component';
import { OffresComponent } from './../offres/offres.component';
import { Routes } from '@angular/router';

export default [
    {
        path: '',
        component: HomeComponent,
        title: 'Home',
        children: [
            {
                path: 'offres',
                loadComponent: () => import('../offres/offres.component').then(c => c.OffresComponent)
            },
            {
                path: 'list-items',
                loadComponent: () => import('../list-items/list-items.component').then(c => c.ListItemsComponent)
            },
            {
                path: 'reviews',
                loadComponent: () => import('../reviews/reviews.component').then(c => c.ReviewsComponent)
            },

        ]
    }

] as Routes;

