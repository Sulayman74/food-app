import { CardsOffresComponent } from '../offres/cards-offres/cards-offres.component';
import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider'
import { MatProgressBarModule } from '@angular/material/progress-bar'
import { OffresComponent } from '../offres/offres.component';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'app-card',
  standalone: true,
  imports: [RouterModule, CommonModule,
    MatButtonModule,
    MatProgressBarModule, MatDividerModule, OffresComponent],
  templateUrl: './card.component.html',
  styleUrl: './card.component.scss'
})
export class CardComponent {
  menusBought = 0;
  get progress() {
    return (this.menusBought / 10) * 100;
  }

  buyMenu() {
    if (this.menusBought < 10) {
      this.menusBought++;
    } else {
      alert('Vous avez gagné un menu gratuit!');
      this.menusBought = 0;
    }
  }
}
