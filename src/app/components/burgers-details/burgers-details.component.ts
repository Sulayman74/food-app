import { Component, Input } from '@angular/core';

import { BurgerInterface } from '../../utils/burger.interface';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';

@Component({
  selector: 'app-burgers-details',
  standalone: true,
  imports: [CommonModule, MatButtonModule, MatCardModule, MatIconModule],
  templateUrl: './burgers-details.component.html',
  styleUrl: './burgers-details.component.scss'
})
export class BurgersDetailsComponent {
  @Input() burger!: BurgerInterface;
}
