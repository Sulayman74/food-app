import { SidesInterface } from "../../utils/side.interface";

export const Sides: SidesInterface[] = [{
    name: 'Tenders', description: 'Tenders de poulet pané', price: '3.00', imageUrl: '/assets/images/tenderssides.jpg', unity: 3
}, {
    name: 'Wings', description: 'Ailes de poulet', price: '3.00', imageUrl: '/assets/images/mixsides.jpg', unity: 4
}, {
    name: 'Nuggets', description: 'Nuggets de poulet', price: '3.00', imageUrl: '/assets/images/nuggets.jpg', unity: 5
}, {
    name: 'Jalapenos', description: 'Bouchée fromage et jalapenos', price: '3.00', imageUrl: '/assets/images/jalapenos.jpg', unity: 4
}, {
    name: 'Bouchée Camembert', description: 'Bouchée au Camembert pané', price: '3.00', imageUrl: '/assets/images/camembert.jpg', unity: 5
}, {
    name: 'Mozza Sticks', description: 'Sticks de mozza pané', price: '3.00', imageUrl: '/assets/images/sticks.jpg', unity: 5
}, {
    name: 'Frites Cheddar Oignon', description: 'Frites au cheddar et oignons frits', price: '3.50', imageUrl: '/assets/images/friesCO.jpg'
}, {
    name: 'Frites Cheddar Oignon Bacon', description: 'Frites au cheddar et oignons frits et bacon', price: '4.00', imageUrl: '/assets/images/friesCOB.jpg', unity: 3
}, {
    name: 'Onions Rings', description: "Rondelle d'oignon frit", price: '3.50', imageUrl: '/assets/images/rings.jpg', unity: 5
}, {
    name: 'Frites', description: 'Barquette de frites', price: '2.00', imageUrl: '/assets/images/fries.jpg'
},
{
    name: 'Croc', description: 'croque-monsieur fromage, jambon, gratiné', price: '2.00', imageUrl: '/assets/images/croc.jpg'
},
]