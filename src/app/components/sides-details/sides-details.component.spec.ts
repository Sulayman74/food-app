import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SidesDetailsComponent } from './sides-details.component';

describe('SidesDetailsComponent', () => {
  let component: SidesDetailsComponent;
  let fixture: ComponentFixture<SidesDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SidesDetailsComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SidesDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
