import { Component, Input } from '@angular/core';

import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { SidesInterface } from '../../utils/side.interface';

@Component({
  selector: 'app-sides-details',
  standalone: true,
  imports: [CommonModule, MatButtonModule, MatCardModule, MatIconModule],
  templateUrl: './sides-details.component.html',
  styleUrl: './sides-details.component.scss'
})
export class SidesDetailsComponent {
  @Input() side!: SidesInterface;
}
