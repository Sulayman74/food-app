import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';

@Component({
  selector: 'app-cart',
  standalone: true,
  imports: [CommonModule, MatButtonModule],
  templateUrl: './cart.component.html',
  styleUrl: './cart.component.scss'
})
export class CartComponent {
  items = [
    { name: 'Produit 1', price: 29.99 },
    { name: 'Produit 2', price: 49.99 },
    { name: 'Produit 3', price: 19.99 }
  ];

  removeItem(item: any) {
    this.items = this.items.filter(i => i !== item);
  }

  getTotal() {
    return this.items.reduce((total, item) => total + item.price, 0);
  }
}
