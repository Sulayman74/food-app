import { Component, ViewChild } from '@angular/core';
import { MatDrawer, MatSidenavModule } from '@angular/material/sidenav';
import { RouterLink, RouterModule, RouterOutlet } from '@angular/router';

import { CommonModule } from '@angular/common';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { ListItemsComponent } from './components/list-items/list-items.component';
import { MatButtonModule } from '@angular/material/button';
import { OffresComponent } from './components/offres/offres.component';
import { ReviewsComponent } from './components/reviews/reviews.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, HeaderComponent, ListItemsComponent, MatSidenavModule, MatButtonModule, FooterComponent, ReviewsComponent, OffresComponent, RouterLink, RouterModule, CommonModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {
  title = 'food-app';
  carteFidelite: boolean = false
  @ViewChild('drawer')
  drawer!: MatDrawer;


  showCard(): void {
    this.carteFidelite = !this.carteFidelite
    console.log("etat", this.carteFidelite);
  }
}
