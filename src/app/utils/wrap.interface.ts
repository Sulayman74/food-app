export interface WrapInterface {
    name: string;
    description: string;
    price: string;
    imageUrl: string;
}