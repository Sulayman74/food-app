export interface Product {
    name: string;
    description: string;
    price: number;
    imageUrl: string;
}

export const PRODUCTS: Product[] = [
    {
        name: 'Tacos',
        description: 'Nos délicieux tacos',
        price: 8.99,
        imageUrl: '/assets/images/tacos.jpg'
    },
    {
        name: 'Burger',
        description: 'Decouvrez nos burgers',
        price: 6.99,
        imageUrl: '/assets/images/burger.jpg'
    },
    {
        name: 'Wraps',
        description: 'Nos Wraps',
        price: 7.49,
        imageUrl: '/assets/images/wrap2.jpg'
    },
    {
        name: 'Sides',
        description: 'Accompagnements',
        price: 7.49,
        imageUrl: '/assets/images/sides.jpg'
    },
];