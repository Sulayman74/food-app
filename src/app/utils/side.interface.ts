export interface SidesInterface {
    name: string;
    description: string;
    price: string;
    imageUrl: string;
    unity?: number;
}