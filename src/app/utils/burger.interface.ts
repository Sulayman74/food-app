export interface BurgerInterface {
    name: string;
    description: string;
    price: string;
    imageUrl: string;
}