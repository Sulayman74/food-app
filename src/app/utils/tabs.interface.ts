// tab.interface.ts
export interface TabInterface {
    label: string;
    content: any; // This could be a component or raw HTML depending on your needs
}