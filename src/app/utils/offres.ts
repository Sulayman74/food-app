export interface Offer {
    image: string;
    title: string;
    description: string;
}

export const OFFERS: Offer[] = [
    {
        image: 'https://scontent-mrs2-2.xx.fbcdn.net/v/t39.30808-6/447757191_1063767738533914_3267337134098996095_n.jpg?_nc_cat=106&ccb=1-7&_nc_sid=5f2048&_nc_ohc=LIGumSCwBO8Q7kNvgGLwLt_&_nc_ht=scontent-mrs2-2.xx&oh=00_AYC84Eqaj49bOTjtq78icBTU31pEBtKl9R7JrN0vPEKGTg&oe=667E79A3',
        title: 'Burger ',
        description: 'Un délicieux burger.'
    },
    {
        image: 'https://scontent-mrs2-2.xx.fbcdn.net/v/t39.30808-6/448773655_1074791890764832_460434129832526643_n.jpg?_nc_cat=108&ccb=1-7&_nc_sid=5f2048&_nc_ohc=qsym2t7yy0IQ7kNvgHBDAWQ&_nc_ht=scontent-mrs2-2.xx&oh=00_AYAN-vS6e_WmyWC6wE7OLeySG2GTZguSDmWdUg3jrTTNNg&oe=667E6814',
        title: 'Tacos',
        description: 'Tacos avec fromage extra.'
    },
    {
        image: '/assets/images/wraps3.jpg',
        title: 'Wrap',
        description: 'Wrap Chicken.'
    },
    {
        image: 'https://scontent-mrs2-1.xx.fbcdn.net/v/t39.30808-6/438058665_1026019468975408_5781163902143496334_n.jpg?_nc_cat=109&ccb=1-7&_nc_sid=5f2048&_nc_ohc=DpGmuQJE8hQQ7kNvgHX7WrC&_nc_ht=scontent-mrs2-1.xx&oh=00_AYCKUdBoetyVbZzCwRQQBBB6Yh0sB4plNN2Ihtt0RX6XdQ&oe=667E573B',
        title: 'Sides',
        description: 'Des tenders de poulet croustillants.'
    }
];