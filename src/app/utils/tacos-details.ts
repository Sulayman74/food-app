export interface TacosInterface {
    name: string;
    description: string;
    price: string;
    imageUrl: string;
}