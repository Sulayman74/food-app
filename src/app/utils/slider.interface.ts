export interface SlideInterface {
    avatarUrl?: string;
    username?: string;
    title?: string;
    review?: string;
    stars: number;
}